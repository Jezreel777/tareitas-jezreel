import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { RegisterService } from './register.service';
import { Usuario } from './interfaces';
import {MatSnackBar} from '@angular/material/snack-bar';
interface Perfil {
  idp: number;
  perf: string;
}
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  userForm : FormGroup

  perfiles = [
    {
      id: 1,
      perfil :'alumnos'
    },
    {
      id: 2,
      perfil :'maestros'
    },
    {
      id: 3,
      perfil :'empresas'
  
    }
  ];
  carreras=[
    { id: 1, carrera: 'TSU Desarrollo de Negocios.'},
    { id: 2, carrera: 'TSU Agricultura Sustentable y Protegida.'},
    { id: 3, carrera: 'TSU Diseño y Moda Industral.'},
    { id: 4, carrera: 'TSU Energías Renovables.'},
    { id: 5, carrera: 'TSU Gastronomía.'},
    { id: 6, carrera: 'TSU Mecatrónica.'},
    { id: 7, carrera: 'TSU Procesos Alimentarios.'},
    { id: 8, carrera: 'TSU Tecnologías de la Información.'},
    { id: 9, carrera: 'ING  Desarrollo de Negocios.'},
    { id: 10, carrera: 'ING Agricultura Sustentable y Protegida.'},
    { id: 11, carrera: 'ING Diseño y Moda Industral.'},
    { id: 12, carrera: 'ING Energías Renovables.'},
    { id: 13, carrera: 'ING Gastronomía.'},
    { id: 14, carrera: 'ING Mecatrónica.'},
    { id: 15, carrera: 'ING Procesos Alimentarios.'},
    { id: 16, carrera: 'ING Tecnologías de la Información.'},
  ];
  constructor(
    private addS: RegisterService,
    private _formBuilder: FormBuilder,
    private _snackBar: MatSnackBar
    ) { }
  UserModel = new Usuario(0,'','','','',true,0,'','','','','','','','','');
  ngOnInit(): void {

    this.userForm = this.crearUserForm();
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action , {
      verticalPosition: 'bottom',
                duration: 2000
    });
  }

  crearUserForm(): FormGroup {
    return this._formBuilder.group({
         nombre: ['',Validators.required],
         telefono:  ['',Validators.required],
         email: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$")]],
         password:['', [ Validators.required, Validators.minLength(8), Validators.maxLength(8), Validators.pattern("(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8}")]],
         activo : [false],
         perfil: [0,[Validators.required] ],
         matricula:[''],
         ape_p: ['', Validators.required],
         ape_m: [''],
         rfc: [''],
         n_control: [''],
         carrera:['',[Validators.required]], 
         imagen: [''],
         ubicacion: [''],
         descripcion:[''], 

    })
  }
   //Contraseña
   hide = true;

   //Agregar usuario
   newUser(){
    console.log(this.userForm);
   this.addS.addUser(this.userForm.value).subscribe(
      adduser =>{
        console.log(adduser);
        this.openSnackBar('Registro completado', 'OK')
      })
  }
  // cambio de perfil
  cambio(j){
    if( j === 1){
      this.userForm.get('matricula').setValidators([Validators.required,Validators.minLength(10),Validators.maxLength(10),Validators.pattern("[A-Z]{4}[0-9]{6}")]);
    } else{
      this.userForm.get('matricula').clearValidators();
      this.userForm.get('matricula').reset();
    }if (j === 2 || j === 3) {
      this.userForm.get('rfc').setValidators([Validators.required,Validators.minLength(13),Validators.maxLength(13),Validators.pattern("[A-Z]{4}[0-9]{6}[a-zA-Z0-9-]{3}")]);
    } else {
      this.userForm.get('rfc').clearValidators();
      this.userForm.get('rfc').reset();
    }if (j === 2 ){
      this.userForm.get('n_control').setValidators(Validators.required);
    }else{
      this.userForm.get('n_control').clearValidators();
      this.userForm.get('n_control').reset();
    }
  }


}
